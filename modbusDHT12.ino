#define DHT12_ADDRESS  ((uint8_t)0x5C)

#define DHT12_OK              (int8_t)0
#define DHT12_ERROR_CHECKSUM  (int8_t)-10
#define DHT12_ERROR_CONNECT   (int8_t)-11
#define DHT12_MISSING_BYTES   (int8_t)-12

#include <ArduinoRS485.h> // ArduinoModbus depends on the ArduinoRS485 library
#include <ArduinoModbus.h>
#include <Wire.h>

struct climate {
  float humidity = 0.0;
  float temperature = 0.0;
  int DHT_Status = 0;
};

void setup() {
  Serial.begin(9600);
  while (!Serial);
  Wire.begin();

  //   start the Modbus RTU server, with (slave) id 42
  if (!ModbusRTUServer.begin(42, 9600)) {
    Serial.println("Failed to start Modbus RTU Server!");
    while (1);

  }
  ModbusRTUServer.configureInputRegisters(0x00, sizeof(climate));
}

void loop() {
  // poll for Modbus RTU requests
  ModbusRTUServer.poll();
  climate clim;


  clim = measure();
  if (DHT12_OK == clim.DHT_Status) {
    union {
      float float_variable;
      unsigned int temp_array[2];
    } u;
    u.float_variable = clim.temperature;
    ModbusRTUServer.inputRegisterWrite(0, u.temp_array[0]);
    ModbusRTUServer.inputRegisterWrite(1, u.temp_array[1]);
    u.float_variable = clim.humidity;
    ModbusRTUServer.inputRegisterWrite(2, u.temp_array[0]);
    ModbusRTUServer.inputRegisterWrite(3, u.temp_array[1]);
    ModbusRTUServer.inputRegisterWrite(4, clim.DHT_Status);
  }
}

climate measure() {
  climate cl;
  uint8_t bits[5];

  Wire.beginTransmission(DHT12_ADDRESS);
  Wire.write(0);
  int rv = Wire.endTransmission();
  if (rv < 0) {
    //    return rv;
  }

  int bytes = Wire.requestFrom(DHT12_ADDRESS, (uint8_t)5);
  if (bytes == 0) {
    cl.DHT_Status = DHT12_ERROR_CONNECT;
    return cl;
  }
  if (bytes < (uint8_t)5) {
    cl.DHT_Status = DHT12_MISSING_BYTES;
    return cl;
  }
  for (int i = 0; i < bytes; i++)   {
    bits[i] = Wire.read();
  }

  cl.humidity = bits[0] + bits[1] * 0.1;
  cl.temperature = (bits[2] & 0x7F) + bits[3] * 0.1;
  if (bits[2] & 0x80) {
    cl.temperature = -cl.temperature;
  }

  uint8_t checksum = bits[0] + bits[1] + bits[2] + bits[3];
  if (bits[4] != checksum) {
    cl.DHT_Status = DHT12_ERROR_CHECKSUM;
    return cl;
  }
  return cl;
}
